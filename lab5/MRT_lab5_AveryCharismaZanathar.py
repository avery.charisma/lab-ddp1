#Lab05#
#Nama:Avery Charisma Zanathar
#NPM: 2106656283
#Kelas: DDP1

# fungsi untuk print integers (bilangan bulat)
def format_number(x):
    if x % 1 != 0:
        pass
    else:
        x = int(x)
    return x
    
#fungsi untuk mendapatkan matriks yang akan digunakan untuk setiap operasi
def get_matrix(index):
    while True:
        try:
            row, col = map(int, input(f"Ukuran matriks {index}, contoh ukuran matriks (2 x 4) diketik 2 4: ").strip().split(' '))
            while True:
                print(f'Barisan matriks {index}:')
                matrix = [[format_number(float(x)) for x in input().strip().split(' ')] for _ in range(row)]
                check_col = [len(matrix[x]) for x in range(len(matrix))]
                # check panjang matriks
                for x in range(len(matrix)):
                    if len(matrix[x]) != col:
                        check_col = False
                    else:
                        pass
                if len(matrix) != row or check_col is False:
                    print("Masukkan sesuai dimensi anda enter.")
                    matrix.clear()
                    continue
                else:
                    break
            return row, col, matrix
        except ValueError:
            print("Please enter a valid size (rows, columns).")

#fungsi untuk print hasil matriks dari setiap operasi
def print_matrix(matrix):
    print("Hasil dari operasi:")
    for row in matrix:
        print(*row)
    print()


def empty_matrix(row, col):
    matrix = [[0 for _ in range(col)] for _ in range(row)]
    return matrix

#fungsi operasi penjumlahan matriks
def add_matrices():
    row1, col1, matrix1 = get_matrix('1')
    row2, col2, matrix2 = get_matrix('2')
    if row1 == row2 and col1 == col2:
        result = [[a + b for a, b in zip(j, l)] for j, l in zip(matrix1, matrix2)]
        print_matrix(result)
    else:
        print()
        print("Terjadi kesalahan input. Silakan ulang kembali.\n")

#fungsi operasi pengurangan matriks
def subtract_matrices():
    row1, col1, matrix1 = get_matrix('1')
    row2, col2, matrix2 = get_matrix('2')
    if row1 == row2 and col1 == col2:
        result = [[a - b for a, b in zip(j, l)] for j, l in zip(matrix1, matrix2)]
        print_matrix(result)
    else:
        print()
        print("Terjadi kesalahan input. Silakan ulang kembali.\n")

#fungsi operasi perkalian matriks
def multiply_matrices():

    row1, col1, matrix1 = get_matrix('1')
    row2, col2, matrix2 = get_matrix('2')
    if col1 == row2:
        result = empty_matrix(row1, col2)
        for i in range(len(matrix1)):
            # iterates through rows of matrix1
            for j in range(len(matrix2[0])):
                # iterates through columns of matrix2
                for k in range(len(matrix2)):
                    # iterates through rows of matrix2
                    result[i][j] += matrix1[i][k] * matrix2[k][j]
        print_matrix(result)
    else:
        print()
        print("Terjadi kesalahan input. Silakan ulang kembali.\n")


#fungsi operasi transpose matriks
def transpose_matrix():
    while True:
        try:
            row, col, matrix = get_matrix('')
            result = [[j for j in i] for i in zip(*matrix)]
            print_matrix(result)
            break
        except len(matrix) != row:
                print("Please enter a matrix with the same dimensions as you entered.")
                continue


# fungsi untuk digunakan pada operasi determinan
def get_cofactor(matrix, count):
    new_matrix = list()
    for i in range(1, len(matrix[1:]) + 1):
        new_row = matrix[i][:count] + matrix[i][count+1:]
        new_matrix.append(new_row)
    return new_matrix

# fungsi operasi determinan matriks
def determinant(matrix):
    if len(matrix) == 1:
        return matrix[0][0]
    if len(matrix) == 2:
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]
    sum_cofactors = 0
    for column in range(len(matrix[0])):
        sign = (-1) ** (column + 2)
        sub_determinant = (determinant(get_cofactor(matrix, column)))
        sum_cofactors += (sign * matrix[0][column] * sub_determinant)
    return sum_cofactors

# main menu user input
def main():
    while True:
        print("Selamat datang di Matrix Calculator. Berikut adalah operasi-operasi yang \ndapat dilakukan:")
        print("1. Penjumlahan\n2. Pengurangan\n3. Transpose\n4. Determinan\n5. Perkalian\n6. keluar")
        u_choice = input("Silakan pilih operasi: ")
        print("\n")
        if u_choice == "1":
            add_matrices()
        elif u_choice == "2":
            subtract_matrices()
        elif u_choice == "3":
            transpose_matrix()
            continue
        elif u_choice == "4":
            while True:
                row, col, matrix = get_matrix('')
                if row == col:
                    print("Hasil dari operasi: ")
                    print(determinant(matrix))
                    print()
                    break
                else:
                    print("Must be a square matrix.\n")
                    continue            
        elif u_choice == "5":
            multiply_matrices()
            continue
        elif u_choice == "6":
            print("\nSampai Jumpa!")
            print()
            break
        else:
            print("Terjadi kesalahan input. Silakan ulang kembali.\n")
            continue
# pemanggilan def main yang merupakan main menu input user
if __name__ == "__main__":
    main()