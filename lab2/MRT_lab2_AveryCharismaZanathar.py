##Tugas Lab02##
#Nama  = Avery Charisma Zanathar
#Kelas = DDP-1
#NPM   = 2106656283

player_hp = 100
raiden_hp = 100
raiden_attack = 20
player_attack = 20
player_ultimate = 50
raiden_ultimate = 50
battle = 1
ronde = 1
play_again = True

while raiden_hp>0:
    print("Hp Ei:", player_hp)
    print("Hp Raiden:", raiden_hp)
    print("Putaran ke", ronde)
    print("-------------------------------")
    print("Apa yang ingin anda lakukan? ")
    print("1. Menyerang \n2. Menghindar \n3. Musou no Hitotachi")
    choices = str(input("Masukkan pilihan anda: "))
    print("-------------------------------")
    if choices == "1":
        raiden_hp = raiden_hp - player_attack
        player_hp = player_hp - raiden_attack
        print("Ei menyerang Raiden dan mengurangi Hp Raiden sebesar",player_attack)
        print("Raiden menyerang Ei dan mengurangi Hp Ei sebesar ",raiden_attack)
        ronde = ronde + 1
        continue
    elif choices == "2":
        print("Raiden menyerang namun Ei berhasil Menghindar")
        print("-------------------------------")
        ronde = ronde + 1
        continue
    elif choices == "3":
        raiden_hp = raiden_hp - player_ultimate
        player_hp = player_hp - raiden_attack
        ronde = ronde + 1
        print("Ei menyerang Raiden dengan","Musou no Hitotachi","dan mengurangi Hp Raiden sebesar", player_ultimate)
        print("Raiden menyerang Ei dan mengurangi Hp Ei sebesar ",raiden_attack)
    elif raiden_ultimate == 3*ronde:
        player_hp = player_hp - raiden_ultimate
        raiden_hp = raiden_hp - player_attack
        print("Raiden menyerang Ei dan mengurangi Hp Ei sebesar", raiden_ultimate)

if raiden_hp <= 0:
    print("-------------------------------")
    print("Pertarungan dimenangkan oleh Ei")
    print()
    play_again = False

if player_hp <= 0:
    print("-------------------------------")
    print("Pertarungan dimenangkan oleh raiden")
    print()
    play_again = False


if player_hp == raiden_hp == 0:
    print("-------------------------------")
    print("Pertarungan seri")
    print()
    play_again = False