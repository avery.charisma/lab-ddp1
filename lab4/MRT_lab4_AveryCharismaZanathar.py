#Lab04#
#Nama:Avery Charisma Zanathar
#NPM: 2106656283
#Kelas: DDP1

import os

deskripsi = """

Text Files and Exceptions:
Ini adalah script program untuk membetulkan document anda dalam format '.txt' (text).
Memisahkan dari karakter selain Alphabet dan Numeric.

Created by: Avery Charisma Zanathar

"""
print(deskripsi) # untuk print tampilan deskripsi

namafile_i = input("Masukkan nama file input: ") # untuk memasukkan nama input file ke dalam variabel
namafile_o = input("Masukkan nama file output: ") # untuk memasukkan nama output file ke dalam variabel
print("\n") #pemisah
cekfile = os.path.isfile(str(namafile_i)) # untuk melakukan checking file
if cekfile: # jika file input ada di dalam folder maka sistem eksekusi proses
    with open(namafile_i, "r") as y: #untuk membuka dan membaca text di dalam file input
        isi = y.read() # membaca dan menerjemahkan isi dalam file input menjadi string dan dimasukkan ke dalam variabel isi
    i = '' # string kosong untuk menyimpan hasil filter
    inum = ''
    for x in isi: # loop untuk memisahkan setiap alphabet
        if x in "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\n ": #jika character masuk ke dalam Alphabet, Numeric, Enter, dan Spasi. maka character di simpan ke dalam i
            i += x #menyimpan hasil filter ke dalam i
        if x in "1234567890\n": #loop untuk memisahkan setiap number
            inum += x

    res = i.split("\n") # split, fungsinya untuk memisahkan object alphabet dengan menggunakan argument yang sudah di tentukan
    res2 = inum.split("\n") # split, fungsinya untuk memisahkan object number dengan menggunakan argument yang sudah di tentukan
    data = open("summary.txt", "a")# open data summary
    data.write("\nNama File Input: "+str(namafile_i))# menulis nama input file di dalam summary
    data.write("\n***")#pemisah biar terlihat cantik
    f = open(namafile_o, "w") #melakukan open dan overwrite untuk membuat file output
    print ("Total baris dalam file input: ",len(res))#menampilkan total baris dalam file input di terminal
    sv = {} #data dict, fungsinya untuk membuat daftar, yang dimana alphabet berperan sebagai key dan number sebagai value
    for enm, axyz in enumerate(res): #loop untuk memasukkan alphabet dari dalam list 'res' ke dalam dict 'sv'
        sv[axyz] = 0 #ini adalah path dari key alphabet yang sudah di buat, value nya 0 karena akan di tambah dengan angka yang ada dalam value alphabet
        for sid in str(res2[enm]):#loop angka, ubah dari type string menjadi int
            sv[axyz] += int(sid)#menjumlahkan seluruh angka yang ada dalam value alphabet
    
    disimpan = 0 #seluruh total dari total angka genap yang ada dalam value alphabet
    for e2ee in sv: #mengambil key dari dalam data dict 'sv'
        if sv[e2ee] % 2 == 0:#memastikan jika value dari e2ee adalah genap
            disimpan += 1 #jika genap maka point dari 'disimpan' akan di tambahkan 1
            f.write(f"{e2ee}\n") #alphabet (key) akan di tulis ke dalam file output
            data.write(f"\n{e2ee}") #alphabet (key) akan di tulis ke dalam file summary

    print ("Total baris yang disimpan: ",disimpan)#menampilkan berapa banyak baris yg di simpan di terminal
    
    data.write("\n***")#pemisah biar terlihat rapi
    data.write("\nTotal baris dalam file input: "+str(len(res))) #menampilkan total baris di terminal
    data.write("\nTotal baris yang disimpan: "+str(disimpan)) #menampilkan total data di simpan di terminal
    data.write("\n***")#pemisah supaya terlihat rapi
    
    data.write("\n-----------------")#pemisah di summary.txt
    data.close()#close proccess read dan write summary.txt
    f.close()
    print("\n")#pemisah supaya terlihat rapi
else:#jika nama file tidak ada di dalam folder maka akan di infokan ke dalam terminal
    print("Nama file yang anda masukkan tidak ditemukan :(")# print info nama file tidak di temukan di terminal
    print("\n")#pemisah supaya terlihat rapi
    
