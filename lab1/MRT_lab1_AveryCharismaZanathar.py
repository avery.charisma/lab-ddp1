#Nama : Avery Charisma Zanathar
#NPM : 2106656283
#Kelas : DDP1

while True:
    try:
        character_string = str(input("Masukkan karakter: "))
        ordinal = ord(character_string)
        print()
        encryption = (ordinal) % 7 * ((ordinal) ** 2) % 69 + 666
        print("Fake Eligma untuk " + character_string +": " + str(encryption))

        binary_x = bin(encryption)
        hexadecimal_x = hex(encryption)
        new_binary = binary_x[2:]
        new_hexadecimal = hexadecimal_x[2:]
        total = new_binary + new_hexadecimal
        print()
        print("Enkripsi Fake Eligma untuk " + character_string + ": " + total)
        print()
        break

    except (TypeError, ValueError):
        print("Oops! Terdapat Error. Harap masukkan sebuah karakter saja ya :)")
        print()
