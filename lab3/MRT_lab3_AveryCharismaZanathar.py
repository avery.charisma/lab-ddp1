## Lab 03 ##
#Nama: Avery Charisma Zanathar
#NPM: 2106656283
#Kelas: DDP1

# import random untuk posisi kunci
import random

#Paragraf dari modul soal
paragraf = "the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow.\
 pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly.\
 how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz.\
 the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog.\
 watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering,\
 jocks fumble the pizza. the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud.\
 sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf.\
 jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim.\
 two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives.\
 a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize!\
 waxy and quivering, jocks fumble the pizza. "

#input perintah user = pesan dan nilai n (sandinya)
if __name__ == "__main__":
    while True:
        #kode masukan (input) user = pesan dan nilai n
        message = str(input("Masukkan pesan yang akan dikirim:\n"))
        nilai_n = int(input("Pilih nilai n: "))
        #mengubah integer nilai n menjadi string untuk perintah ketika terlalu panjang karakternya
        string_n = str(nilai_n)
        print()
        #membuat pesannya menjadi huruf kecil semua
        message = message.lower()
        hasil_string = ""
        #perintah ketika karakter yang dimasukkan terlalu panjang
        if len(message+string_n)>35:
            print("Terlalu panjang. Maksimal 32 karakter")
            exit(0)
        
        #rumus untuk caesar encryption
        for letter in range(len(message)):
            if message[letter].isnumeric():
                cipher = (ord(message[letter]) + nilai_n - 48) % 10 + 48
                cipher = (cipher + nilai_n - 48) % 10 + 48
                hasil_string += chr(cipher)
            elif message[letter] == ' ':
                hasil_string += message[letter]
            else: #menggeserkan tiap karakter berdasarkan nilai n yang dimasukkan user
                cipher = (ord(message[letter]) + nilai_n - 97) % 26 + 97
                hasil_string += chr(cipher)

        cipher_paragraf = ""
        letter = 0
        #proses membuat kunci beserta posisinya
        try:
            index = 0
            #random posisi kunci sesuai modul soal
            posisi_n = random.randint(100, len(paragraf)-100)
            #rumus kunci nilai (20220310 - nilai n)
            rumus_kunci = 20220310 - nilai_n
            #finalisasi kunci n
            kunci_n = '*' + str(rumus_kunci) + '*'
            #proses menyelipkan pesan user dimasukkan kedalam paragraf
            while letter < len(paragraf):
                if index < len(hasil_string):
                    if hasil_string[index].isnumeric():
                        cipher_paragraf += hasil_string[index]
                        index += 1
                        letter -= 1
                    elif hasil_string[index] == " ":
                        cipher_paragraf += '$'
                        index += 1
                        letter -= 1
                    else:
                        if hasil_string[index] == paragraf[letter]:
                            cipher_paragraf += paragraf[letter].upper()
                            index += 1
                        else:
                            cipher_paragraf += paragraf[letter]
                else:
                    cipher_paragraf += paragraf[letter]
                letter += 1
        except (NameError, ValueError, TypeError):
            print("Error! Tidak bisa diproses")
            exit(0)
        #Memasukkan kunci ke dalam paragraf sesuai permintaan modul = posisi random
        cipher_paragraf = cipher_paragraf[:posisi_n] + kunci_n + cipher_paragraf[posisi_n:]
        # print hasil enkripsi paragraf beserta pesannya
        print(cipher_paragraf)
        print()
        exit(0)